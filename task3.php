<?php

function rewriteJsonFile(string $pathToJsonFile, string $key, $value): void {
    // JSON файл лежит по пути $pathToJsonFile. Необходимо получить содержимое этого файла,
    // добавить в него поле $key со значением $value и перезаписать.

    // читаем файл
    $orig = file_get_contents($pathToJsonFile);

    //переводим в php массив
    $arr = json_decode($orig, true);

    //добавляем ключ
    $arr[$key] = $value;

    //кодируем обратно в json, записываем обратно
    file_put_contents($pathToJsonFile, json_encode($arr));
}