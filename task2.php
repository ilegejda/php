<?php

function createFileWithSum(string $pathToFiles): void {
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles

    // Проверка есть ли такой путь
    try {

        $rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($pathToFiles));

        $sum = [];

        // перебираем файлы
        foreach ($rii as $file) {

            // отбрасываем директории
            if ($file->isDir()){
                continue;
            }

            // если есть содержимое
            if (file_get_contents($file->getPathname())) {

                $i = 0;
                // открываем для чтения
                $handle = @fopen($file->getPathname(), "r");
                if ($handle) {
                    // перебираем строки
                    while (($buffer = fgets($handle, 4096)) !== false) {
                        // если число
                        if ((int)$buffer){
                            // если есть такой ключ в массиве
                            if (isset($sum[$i]))
                            {
                                $sum[$i] += $buffer;
                            }
                            else
                            {
                                $sum[$i] = $buffer;
                            }
                            $i++;
                        }
                    }
                    if (!feof($handle)) {
                        echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
                    }
                    fclose($handle);
                }
            }
        }
        // записываем обратно
        file_put_contents($pathToFiles."/3.txt", implode(PHP_EOL, $sum));
    }
    catch (Exception $e){

    }

}